import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Extras 1.4

ApplicationWindow {
    title:qsTr("Jack Transport")
    visible: true
    SystemPalette { id: maintheme; colorGroup: SystemPalette.Active }
    width: 345
    height: 66
    color: maintheme.window
    opacity: 0.95

RowLayout{    
    anchors.fill: parent
    PlasmaComponents3.ToolButton {
                    id: skipbackbutton
                    Layout.fillWidth: true
                    Layout.minimumHeight: width
                    Layout.minimumWidth: 64
                    icon.name: "media-seek-backward"
                    onClicked: backend.SkipBackButton()
                    
    }

    Connections {
            target: backend
            function onSignalSkipBackButton(boolValue){
                return
            }
            function onRewindButton(boolValue){
                return
            }
    }
        PlasmaComponents3.ToolButton {
                    id: stopbutton
                    Layout.fillWidth: true
                    Layout.minimumHeight: width 
                    Layout.minimumWidth: 64
                    icon.name: "media-playback-stop"
                    onClicked: { backend.StopButton()
//                     playpause.icon.name = "media-playback-start"
                    }
        }
            Connections {
            target: backend
            function onSignalStopButton(boolValue){
                return
            }
    }
    
        PlasmaComponents3.ToolButton {
                    id: play
                    Layout.fillWidth: true
                    Layout.minimumHeight: width 
                    Layout.minimumWidth: 64
                    icon.name: "media-playback-start"
                    onClicked: { backend.PlayButton()                        
                    }
                }
            Connections {
            target: backend
            function onSignalPlayButton(boolValue){
                return
            }
    }
            PlasmaComponents3.ToolButton {
                    id: pause
                    Layout.fillWidth: true
                    Layout.minimumHeight: width 
                    Layout.minimumWidth: 64
                    icon.name: "media-playback-pause"
                    onClicked: { backend.PauseButton()
                    }
                }
            Connections {
            target: backend
            function onSignalPauseButton(boolValue){
                return
            }
    }
            PlasmaComponents3.ToolButton {
                    id: skipforwardbutton
                    Layout.fillWidth: true
                    Layout.minimumHeight: width 
                    Layout.minimumWidth: 64
                    icon.name: "media-seek-forward"
                    onClicked: backend.SkipForwardButton()
                    
    }
    Connections {
            target: backend
            function onSignalSkipForwardButton(boolValue){
                return
            }
            function onFastForwardButton(boolValue){
                return
            }
    }
    
}
}
