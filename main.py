# This Python file uses the following encoding: utf-8
import jack
import sys
import os
from PySide2.QtCore import *
from PySide2.QtQml import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

# Starting and naming the JACK Client.
client = jack.Client('Jack Transport for Plasma')        
        
        
class MainBackend(QObject):
              
   #Skip Back Button. The frames from client.transport_query are formatted and turned into an integer to be subtracted from.
    def __init__(self):
        QObject.__init__(self)
    signalSkipBackButton = Signal(bool)
    @Slot()
    def SkipBackButton(self):
        self.signalSkipBackButton.emit(True)
        position, pos = client.transport_query()
        frames = []
        frames.append(format(
        pos['frame']))
        formattedframes ="".join(frames)
        numberedframes = int(formattedframes)
        if numberedframes > 48000:
            client.transport_frame = (numberedframes - 48000)
        else:
            client.transport_frame = 0

    def __init__(self):
        QObject.__init__(self)
    signalStopButton = Signal(bool)
    @Slot()
    def StopButton(self):
        self.signalStopButton.emit(True)
        client.transport_stop()
        client.transport_frame = 0
    
    def __init__(self):
        QObject.__init__(self)
    signalPlayButton = Signal(bool)
    @Slot()
    def PlayButton(self):
        self.signalPlayButton.emit(True)
        client.transport_start()
            
    def __init__(self):
        QObject.__init__(self)
    signalPauseButton = Signal(bool)
    @Slot()
    def PauseButton(self):
        self.signalPauseButton.emit(True)
        client.transport_stop()
                   

    def __init__(self):
        QObject.__init__(self)
    signalSkipForwardButton = Signal(bool)
    @Slot()
    def SkipForwardButton(self):
        self.signalSkipForwardButton.emit(True)
        position, pos = client.transport_query()
        frames = []
        frames.append(format(
        pos['frame']))
        formattedframes ="".join(frames)
        numberedframes = int(formattedframes)
        client.transport_frame = (numberedframes + 48000)
        
if __name__ == '__main__':
    os.environ["QT_QUICK_BACKEND"] = "software"
    application = QGuiApplication(sys.argv)
    application.setWindowIcon(QIcon("jtfp.png"))
    engine = QQmlApplicationEngine()
    main = MainBackend()
    engine.rootContext().setContextProperty("backend", main)
    engine.load(os.path.join(os.path.dirname(__file__), "main.qml"))
    if not engine.rootObjects():
        sys.exit(-1)
    try:
        sys.exit(application.exec_())
    except:
        pass

