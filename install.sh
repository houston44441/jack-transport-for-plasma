 #!/bin/sh
 
if [ -d ~/.local/share/jtfp ]
then
    echo ".local/share/jtfp found"
else
    mkdir ~/.local/share/jtfp
fi

if [ -d ~/.local/bin ]
then
    echo ".local/bin found"
else
    mkdir ~/.local/bin
fi

if [ -d ~/.local/share/icons ]
then
    echo ".local/share/icons found"
else
    mkdir ~/.local/share/icons
fi

if [ -d ~/.local/share/applications ]
then
    echo ".local/share/applications found"
else
    mkdir ~/.local/share/applications
fi
cp main.py ~/.local/share/jtfp
cp main.qml ~/.local/share/jtfp
cp jtfp.png ~/.local/share/icons
cp jtfp.png ~/.local/share/jtfp
cp jack-transport-for-plasma ~/.local/bin
cp jack-transport-for-plasma.desktop ~/.local/share/applications
echo 'Finished'
