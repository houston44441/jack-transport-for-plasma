# Jack Transport for Plasma
![With Breeze Dark Theme](exampleimage/jacktransportplasma.png)

A Python application with a QML GUI for using with Jack Transport. The GUI uses the theme's icons and application background colour.

## Dependencies

### For Ubuntu Studio 21.10

    sudo apt install python3-pyside2.qtquick python3-pyside2.qtwidgets qml-module-qtquick-extras

## Installation

You can test it by downloading the repository and running it with

    python3 main.py

It can be installed by running the install script

    sh install.sh

## Usage

Running the install script allows it to be opened with the command

    jack-transport-for-plasma

It should also be able to opened from the application menu. The command can be used in session managers like ray-session and agordejo.

It is not an NSM client but it is so simple, that doesn't matter.
